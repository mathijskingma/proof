class CasesChart
	constructor: ->
		@bar_spacing = 3
		@transition_ease = 'bounce'
		@transition_duration = 1000
		@margin = { top: 20, right: 50, bottom: 30, left: 40}
		@width = 850 - @margin.left - @margin.right
		@height = 350 - @margin.top - @margin.bottom

		@svg = d3.select('.cases-graph')
			.append('svg')
			.attr('width', (@width + @margin.left + @margin.right))
			.attr('height', @height + @margin.top + @margin.bottom)
			.append('g')
			.attr('transform', 'translate(' + [@margin.left, @margin.top] + ')')

		# Add graph title
		@svg.append('svg:text')
			.attr('class', 'title')
				.attr('x', 50)
				.attr('y', -10)
				.text('Openstaande zaken')
	draw: ->
		# d3.select('.cases-graph').select('svg').remove()



	update: (data) ->

		# Axis
		@y = d3.scale.linear().range([@height, 0])
		@x = d3.scale.ordinal().
			rangeRoundBands([@margin.left, @width - @margin.right - 100], .1)
			.domain data.map (d) => d.value

		@xAxis = d3.svg.axis()
			.scale(@x)
			.orient('bottom')

		@yAxis = d3.svg.axis()
			.scale(@y)
			.orient('left')
			.ticks(10)

		@y.domain([0, d3.max data, (d) =>
			d.value
		])

		@x.domain(data.map (d) =>
			d.age
		)

		# Append x Axis
		@svg.append('g')
			.attr('class', 'x axis')
			.attr('transform', 'translate(0, '+ @height + ')')
			.call(@xAxis)

		# Append yAxis
		@svg.append('g')
			.attr('class', 'y axis')
			.attr('transform', 'translate('+ @margin.left + ')')
			.call(@yAxis)
			.append('text')
				.attr('transform', 'rotate(-90)')
				.attr('y', -40)
				.attr('dy', '.71em')
				.style('text-anchor', 'end')
				.text('Aantal zaken')

		@bars = @svg.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('class', (d,i) => "rect-" + i)
			.attr('x', (d) => @x(d.age) )
			.attr('y', @height)
			.attr('width', @x.rangeBand())
			.attr('height', 0)
			.style('fill', (d) => d.color)
			.style('fill-opacity', 1)
			.transition()
			.ease(@transition_ease)
			.duration(@transition_duration)
				.attr('y', (d) => @y(d.value))
				.attr('height', (d) => @height - @y(d.value))

		# Legenda
		@legend = @svg.append('g')
			.attr('class', 'legend')
				.attr('x', @width + 65)
				.attr('y', 55)
				.attr('height', 100)
				.attr('width', 100)

		# @legend.selectAll('g')
		# 	.data(data)
		# 	.enter()
		# 	.append('g')
		# 	.each (d, i) ->
		# 		@g = d3.select(this)
		# 		@g.append('rect')
		# 			.attr(x)

	update2: (data) ->
		# @bars = @svg.selectAll('rect').data(data)
		# 	.enter()
		# 	.attr('x', (d) => @x(d.age + 50) )
		# 	.attr('y', (d) => @y(d.value + 50))

		@svg.selectAll('rect').data(data)
			.transition()
			.ease(@transition_ease)
			.duration(500)
				.attr('y', (d) => @y(d.value))
				.attr('height', (d) => @height - @y(d.value))

$ ->
	data = [
	  {
	    age: "Verzending factuur"
	    value: "385"
	    amount: 10000
	    color: "rgba(51, 255, 0, .5)"
	  }
	  {
	    age: "Eerste herinnering"
	    value: "360"
	    color: "blue"
	  }
	  {
	    age: "Tweede herinnering"
	    value: "320"
	    color: "pear"
	  }
	  {
	    age: "Aanmaning"
	    value: "255"
	    color: "gold"
	  }
	  {
	    age: "Herinnering aanmaning"
	    value: "320"
	    color: "yellow"
	  }
	  {
	    age: "Pregerechterlijk"
	    value: "310"
	    color: "red"
	  }
	  {
	    age: "Gerechterlijk"
	    value: "310"
	    color: "silver"
	  }
	  {
	    age: "Einde traject"
	    value: "310"
	    color: "pink"
	  }
	]


	@graph = new CasesChart
	@graph.update(data)

	$('.test').click (e) =>
		data2 = [
		  {
		    age: "Verzending factuur"
		    value: "285"
		    color: "rgba(51, 255, 0, .5)"
		  }
		  {
		    age: "Eerste herinnering"
		    value: "100"
		    color: "blue"
		  }
		  {
		    age: "Tweede herinnering"
		    value: "220"
		    color: "pear"
		  }
		  {
		    age: "Aanmaning"
		    value: "355"
		    color: "gold"
		  }
		  {
		    age: "Herinnering aanmaning"
		    value: "220"
		    color: "yellow"
		  }
		  {
		    age: "Pregerechterlijk"
		    value: "210"
		    color: "red"
		  }
		  {
		    age: "Gerechterlijk"
		    value: "110"
		    color: "silver"
		  }
		  {
		    age: "Einde traject"
		    value: "210"
		    color: "pink"
		  }
		]
		@graph.update2(data2)

