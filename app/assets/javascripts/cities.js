// $( document ).ready(function() {


// 	d3.json("http://localhost:3002/data1.json", function(error, json) {
// 	  if (error) return console.warn(error);
// 	  update(json);
// 	});

// 	function updateData(url) {
// 		d3.json(url, function(error, json) {
// 		  if (error) return console.warn(error);
// 		  rect.data(json)
// 		});
// 	}

// 	function update(dataset) {
// 		var margin = {top: 20, right: 50, bottom: 30, left: 40},
//     width = 850 - margin.left - margin.right,
//     height = 350 - margin.top - margin.bottom;
// 		var y = d3.scale.linear().range([height, 0]);
// 		var x = d3.scale.ordinal().rangeRoundBands([margin.left, width - margin.right-100], .1).domain(dataset.map(function(d) { return d.value}));


// 		// Define xAxis
// 		var xAxis = d3.svg.axis()
// 			.scale(x)
// 			.orient("bottom");

// 		// Define yAxis
// 		var yAxis = d3.svg.axis()
// 			.scale(y)
// 			.orient("left")
// 			.ticks(10);

// 		// Tooltips
// 		var tip = d3.tip()
// 		  .attr('class', 'd3-tip')
// 		  .offset([-10, 0])
// 		  .html(function(d) {
// 		    return "<strong>Zaken:</strong> <span style='color:red'>" + d.value + "</span>";
// 		  })

// 		// Add graph
// 		var svg = d3.select("body")
// 								.append("svg")
// 								.attr("class", "test")
// 								.attr("width", (width + margin.left + margin.right))
// 								.attr("height", height + margin.top + margin.bottom)
// 								.append('g')
// 								.attr("transform", 'translate(' + [margin.left, margin.top] + ')');

// 		svg.call(tip);

// 		// Set scales
// 		y.domain([0, d3.max(dataset, function(d) { return d.value; })]);
// 		x.domain(dataset.map(function(d) { return d.age; }));

// 		var yMax = d3.max(dataset, function(data) {
// 			return d3.max(data, function(d) {
// 				return d.value;
// 			});
// 		});
// 		// Add title
// 		svg.append("svg:text")
// 			.attr("class", "title")
// 			.attr("x", 50)
// 			.attr("y", -10)
// 			.text("Openstaande zaken");

// 		// Append xAxis
// 		svg.append("g")
// 			.attr("class", "x axis")
// 			.attr("transform", "translate(0," + height + ")")
// 			.call(xAxis);

// 		// Append yAxis
// 		var yAxis = svg.append("g")
// 				.attr("class", "y axis")
// 				.attr("transform", "translate(" + margin.left + ")")
// 				.call(yAxis)
// 			.append("text")
// 	      .attr("transform", "rotate(-90)")
// 	      .attr("y", -40)
// 	      .attr("dy", ".71em")
// 	      .style("text-anchor", "end")
// 	      .text("Aantal zaken");

// 		// Bars

// 		var rect = svg.selectAll("rect")
// 				.data(dataset)
// 				.enter()
// 				.append("rect")
// 				.attr("class", function(d, i) {
// 					return ("rect-" + i);
// 				})
// 				.attr("x", function(d, i) {
// 					return x(d.age)
// 				})
// 				.attr("y", function(d) {
// 					return y(d.value);
// 				})
// 				.attr("width", x.rangeBand())
// 				.attr("height", function(d) {
// 					return height - y(d.value);
// 				})
// 				.attr("fill", function(d) {
// 					return d.color;
// 				})
// 				.transition()
// 				.ease('bounce')
// 				.duration(4000)
// 				.attr('height', function(d) {
//       		return height - y(d.value - 200);
//       	})
// 				// .on('mouseover', tip.show)
// 	   //    .on('mouseout', tip.hide);



// 		// Legenda
// 		var legend = svg.append("g")
// 		  .attr("class", "legend")
// 		  .attr("x", width + 65)
// 		  .attr("y", 55)
// 		  .attr("height", 100)
// 		  .attr("width", 100);

// 		legend.selectAll('g').data(dataset)
// 	    .enter()
// 	    .append('g')
// 	    .attr("class", "legend")
// 	    .each(function(d, i) {
// 	      var g = d3.select(this);
// 	      g.append("rect")
// 	        .attr("x", width - 110)
// 	        .attr("y", i * 35+7)
// 	        .attr("width", 20)
// 	        .attr("height", 20)
// 	        .style("fill", d.color)
// 	        .on("mouseover", function() {
// 						svg.selectAll(".rect-" + i).style("opacity", .5);
// 					})
// 	        .on("mouseout", function() {
// 						svg.selectAll(".rect-" + i).style("opacity", 1);
// 					});

// 	      g.append("text")
// 	        .attr("x", width - 80)
// 	        .attr("y", i * 35 + 22)
// 	        .attr("height",30)
// 	        .attr("width",100)
// 	        .style("fill", "#d91f2c")
// 	        .text(d.age)
// 	        .on("mouseover", function() {
// 						svg.selectAll(".rect-" + i).style("opacity", .5);
// 					})
// 	        .on("mouseout", function() {
// 						svg.selectAll(".rect-" + i).style("opacity", 1);
// 					});
// 	    });



// 	    d3.selectAll("input").on("change", change);
// 			function change() {
// 			  if (this.value === "grouped") transitionGrouped();
// 			  else transitionStacked();
// 			}

// 			function transitionGrouped() {
// 			  y.domain([0, yMax]);
// 			  yAxis.text("Aantal zaken")
// 			  updateData('http://localhost:3002/data1.json');
// 			}

// 			function transitionStacked() {
// 			  y.domain([0, yMax]);
// 			  yAxis.text("Bedragen")
// 			  updateData('http://localhost:3002/data2.json');
// 			}
// 		}
// });