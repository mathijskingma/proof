#= require jquery
#= require jquery_ujs
#= require iconic.min
#= require turbolinks
#= require d3.min
#= require d3-tip
#= require_tree .

# Turbolinks.enableTransitionCache()

# iconic = IconicJS pngFallback: 'assets/images/iconic'

# ($ document).on 'page:load', ->
#   iconic.inject '.iconic'