module IconicHelper
  def iconic_tag(glyph, size = 'sm')
    src = image_path("iconic/#{glyph}-#{size}.svg")

    tag('img', class: "iconic", data: { src: src })
  end
end
